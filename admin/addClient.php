<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Drive-NSK</title>
    <?php include 'parts/styles.php';?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <?php include 'parts/navBar.php';?>


            <div class="clearfix"></div>
            <br />

            <!-- sidebar menu -->
            <?php include 'parts/sidebarMenu.php';?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php include 'parts/footerButtons.php';?>

            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include 'parts/topBar.php';?>

        <!-- /top navigation -->

        <?php //include 'php/addClient.php';
        if (isset($_GET['id'])){
          $id = $_GET['id'];
          $client = ORM::forTable('client')->where('id', $id)->findOne();
          $fio = $client['fio'];
          $passport = $client['passport'];
          $who = $client['who'];
          $address = $client['adres'];
          $prava = $client['prava'];
          $phone = $client['phone'];
          $number = $client['id'];
          $birth = $client['birth'];

        }else {
          $fio = '';
          $passport = '';
          $who ='';
          $address = '';
          $prava = '';
          $phone = '';
          $number = '';
          $birth = '';

        }
        ?>
        <?php include 'php/editAdd.php'; ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2 ><?= $title ?></h2>

                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="php/addClient.php">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Ф.И.О.</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="fio" value="<?= $fio ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for=birth">Дата рождения</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="birth" required="required" class="form-control col-md-7 col-xs-12" name="birth" value="<?= $birth ?>" placeholder="гггг-мм-дд">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Паспорт (серия и номер)</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="passport" value="<?= $passport ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Кем, когда выдан</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="who" value="<?= $who ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Адрес регистрации</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="address" value="<?= $address ?>"  >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Водительское удостоверение</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="prava" value="<?= $prava ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Телефон</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="phone-number" required="required" class="form-control col-md-7 col-xs-12" name="phone" value="<?= $phone ?>">
                      </div>
                    </div>
                    <div class="form-group" <?= $hidden?>>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Личный номер</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name"  class="form-control col-md-7 col-xs-12" name="id" value="<?= $number ?>">
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <input type="hidden" value="<?= $id ?>" name="id">

                        <!--                        <button type="submit" class="btn btn-primary">Отменить</button>-->
                        <button type="submit" class="btn btn-success" name="<?= $btnName ?>"><?= $buttonText ?></button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->

        <!-- /footer content -->
      </div>
    </div>

    <?php include 'parts/scripts.php';?>
    <script src="/admin/src/masked.input.js"></script>
    <script type="text/javascript">
      jQuery(function($){
        $("#phone-number").mask("+7 (999) 999-9999");
      });
    </script>

  </body>
</html>
