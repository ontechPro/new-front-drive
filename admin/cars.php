<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Drive-NSK</title>
    <?php include 'parts/styles.php';?>



</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php include 'parts/navBar.php';?>


                <div class="clearfix"></div>
                <br />

                <!-- sidebar menu -->
                <?php include 'parts/sidebarMenu.php';?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <?php include 'parts/footerButtons.php';?>

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php include 'parts/topBar.php';?>

        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Список автомобилей</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <a href="/admin/addCar.php" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Добавить новый </a>

                                <!-- start project list -->
                                <table class="table table-striped projects">
                                    <thead>
                                    <tr>
                                        <th class="w-3">#</th>
                                        <th class="w-7">Модель</th>
                                        <th class="w-3">Номер</th>
                                        <th>Цвет </th>
                                        <th>Двигатель </th>
                                        <th>Мощность </th>
                                        <th>Свидетельство о регистрации ТС </th>
                                        <th style="width: 7%">#Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $cars = ORM::forTable('auto')->findArray();
                                  foreach ($cars as $car){
                                      switch ($car['status']){
                                          case 0:
                                              $status = 'Свободна';
                                              $statusColor = 'success';
                                              $progress = 100;
                                              break;
                                          case 1:
                                              $status = 'В прокате';
                                              $statusColor = 'warning';
                                              $progress = rand(5,98);

                                              break;
                                          case 2:
                                              $status = 'Недоступна';
                                              $statusColor = 'danger';
                                              $progress = 100;

                                              break;
                                      }
                                      $costs = ORM::forTable('closed')->where('znak', $car['znak'])->findArray();
                                      $sum=0;
                                      foreach ($costs as $cost){
                                          $sum +=$cost['sum'];
                                      }
                                        echo '
                                      <tr>
                                        <td>'.$car['id'].'</td>
                                        <td class="w-7">
                                            <a>'.$car['model'].'</a>
                                                    </br>
                                             <small>'.$car['year'].'</small>
                                        </td>     
                                           <td class="w-3">
                                            <a>'.$car['znak'].'</a>

                                        </td> 
                                         <td class="w-3">
                                            <a class="small">'.$car['color'].'</a>

                                        </td>                                          

                                        <td class="w-3">
                                            <a>'.$car['engine'].'</a>
                                        </td>                                        
                                        <td class="w-3">
                                            <a>'.$car['power'].'</a>
                                        </td>
                                        <td class="w-7">
                                            <a>'.$car['svid'].'</a>
                                        </td>';
//                                       echo '<td class="project_progress ">
//                                            <div class="progress progress_sm">
//                                                <div class="progress-bar progress-bar-'.$statusColor.' progress-bar-striped" role="progressbar" data-transitiongoal="'.$progress.'"></div>
//                                            </div>
//                                             <small>Осталось '. (100 - $progress).' %</small>
//
//                                        </td>';
                                        echo '
                                        <td class="w-3">
<!--                                            <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>-->
                                            <a style="width:  100%" href="addCar.php?id='.$car['id'].'" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> Редактировать </a>
                                                                 <button type="button" class="btn btn-default btn-xs w-100" data-toggle="modal" data-target=".bs-example-modal-sm' . $car['id'] . '"><i class="fa fa-trash-o"></i>Удалить</button>

            <div class="modal fade bs-example-modal-sm' . $car['id'] . '" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <form method="post" action="'.$_SERVER['PHP_SELF'].'">
                  <div class="modal-body">
                     <h5 class="modal-title" id="myModalLabel2">Вы уверены, что хотите удалить авто ' . $car['model'] . '?</h5>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-danger" name="submit' . $car['id'] . '">Удалить</button>
                  </div>
                  </form>
                   
                </div>
              </div>
            </div>
                                        </td>
                                    </tr>';
                                      if (isset($_POST['submit' . $car['id']])) {
                                          $client = ORM::forTable('auto')->findOne($car['id']);
                                          $client->delete();
                                          echo '<script> location.reload()</script>';
                                      }
                                    }
                                    ?>

                                    </tbody>
                                </table>
                                <!-- end project list -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- /footer content -->
    </div>
</div>
<?php include 'parts/scripts.php';?>


</body>
</html>
