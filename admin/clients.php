<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Drive-NSK</title>

    <?php include 'parts/styles.php';?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="/admin/" class="site_title"><i class="fa fa-car"></i> <span>DRIVE-NSK</span></a>
            </div>

            <div class="clearfix"></div>
            <br />

            <!-- sidebar menu -->
            <?php include 'parts/sidebarMenu.php';?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php include 'parts/footerButtons.php';?>

            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include 'parts/topBar.php';?>

        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_content">
                <a href="/admin/addClient.php" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Добавить клиента </a>

                <table id="datatable-buttons" class="table table-striped table-bordered">
                  <thead>
                  <tr>
                    <th>Ф.И.О</th>
                    <th>Паспорт</th>
                    <th>Кем выдан</th>
                    <th>Прописка</th>
                    <th>Права</th>
                    <th>Телефон</th>
                    <th>Номер клиента</th>
                    <th>Действия</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $clients = ORM::forTable('client')->findArray();
                  foreach ($clients as $client){

                    echo '<tr>
                    <td class="w_20">'.$client['fio'].'</td>
                    <td class="w-7">'.$client['passport'].'</td>
                    <td class="w_20">'.$client['who'].'</td>
                    <td class="w_20">'.$client['adres'].'</td>
                    <td class="w-7">'.$client['prava'].'</td>
                    <td class="w-7">'.$client['phone'].'</td>
                    <td class="w-3">'.$client['id'].'</td>

                    <td class="w-7">
                      <a  href="addClient.php?id='.$client['id'].'" class="btn btn-default btn-xs w-100"><i class="fa fa-pencil"></i> Редактировать </a>
                      <button type="button" class="btn btn-default btn-xs w-100" data-toggle="modal" data-target=".bs-example-modal-sm' . $client['id'] . '"><i class="fa fa-trash-o"></i>Удалить</button>

            <div class="modal fade bs-example-modal-sm' . $client['id'] . '" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <form method="post" action="'.$_SERVER['PHP_SELF'].'">
                  <div class="modal-body">
                                      <h5 class="modal-title" id="myModalLabel2">Вы уверены, что хотите удалить клиента с именем ' . $client['fio'] . '?</h5>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-danger" name="submit' . $client['id'] . '">Удалить</button>
                  </div>
                  </form>
                   
                </div>
              </div>
            </div>
                    </td>
                  </tr>';
                    if (isset($_POST['submit' . $client['id']])) {
                      $client = ORM::forTable('client')->findOne($client['id']);
                      $client->delete();
                      echo '<script> location.reload()</script>';
                    }
                  }
                  ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- /footer content -->
      </div>
    </div>

    <?php include 'parts/scripts.php';?>

  </body>
</html>