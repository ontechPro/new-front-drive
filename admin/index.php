<!DOCTYPE html>
<?php
require_once 'database/idiorm.php';
if (isset($_POST['submit'])) {
    $order = ORM::for_table('orders')->create();
    $time = $_POST['time'];
    $date = explode(' - ', $_POST['reservation-time']);

    $from = $date[0] . ' ' . $time;
    $to = $date[1] . ' ' . $time;

    $order->znak = $_POST['car'];
    $order->client = $_POST['client'];
    $order->date_from = $from;
    $order->date_to = $to;
    $order->save();

    $carRow = ORM::for_table('auto')->where('znak', $_POST['car'])->find_one();

    $carRow->set('status', 1);
    $carRow->save();
    header('Location: /admin/orders.php');
    exit;
    //  echo '<span  style="font-size: small; color: #26b99a; ">Заявка успешно добавлена</span>';
}

?>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Drive-NSK</title>

    <?php include 'parts/styles.php'; ?>
    <link href="/admin/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/admin/" class="site_title"><i class="fa fa-car"></i> <span>DRIVE-NSK</span></a>
                </div>

                <div class="clearfix"></div>
                <br/>

                <!-- sidebar menu -->
                <?php include 'parts/sidebarMenu.php'; ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <?php include 'parts/footerButtons.php'; ?>

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php include 'parts/topBar.php'; ?>

        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="col-md-6 well col-md-offset-3">
                <h4>Оформление заявки</h4>
                <form class="form-horizontal" method="post">
                    <div class="col-md-12">

                        <div class="control-group">
                            <div class="controls">
                                <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon"><i
                                            class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" name="reservation-time" id="reservation-time"
                                           class="form-control"
                                           value="<?= date("Y-m-d"); ?> - <?= (new DateTime(date("Y-m-d")))->add(new DateInterval("P3D"))
                                               ->format('Y-m-d'); ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <select name="time" id="select-time" class="" style="display: none" required=""
                                    data-placeholder="Выберите время">
                                <option value=""></option>

                                <?php
                                for ($i = 0; $i < 24; $i++) {
                                    strlen($i) < 2 ? $h = "0" . $i : $h = $i;
                                    $name = $h . ":00:00";
                                    echo '<option value="' . $name . '">' . $name . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">

                        <div class="form-group">
                            <select name="client" id="select-client" class="" style="display: none" required=""
                                    data-placeholder="Выберите клиента">
                                <option value=""></option>

                                <?php
                                $users = ORM::forTable('client')->findArray();
                                foreach ($users as $user) {
                                    $name = $user['fio'];
                                    $id = $user['id'];
                                    echo '<option value="' . $name . '">' . $name . ' (' . $id . ')</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <select name="car" id="select-car" style="display: none" required=""
                                    data-placeholder="Выберите машину">
                                <option value=""></option>
                                <?php
                                $cars = ORM::forTable('auto')->findArray();
                                foreach ($cars as $car) {
                                    $name = $car['model'] . ' \ ' . $car['color'] . ' \ ' . $car['znak'];
                                    echo '<option value="' . $car['znak'] . '">' . $name . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success" name="submit">Сохранить</button>
                    </div>
                </form>


            </div>

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- /footer content -->
    </div>
</div>

<?php include 'parts/scripts.php'; ?>
<script>
    $(document).ready(function () {
        $('#reservation').daterangepicker(null, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#reservation-time').daterangepicker({
            // timePicker: true,
            //timePickerIncrement: 30,

            locale: {
                format: 'YYYY-MM-DD',
                language: 'ru'
            }
        });
    });
</script>
<script type="text/javascript" src="dist/js/standalone/selectize.js"></script>
<link rel="stylesheet" type="text/css" href="dist/css/selectize.css"/>
<script>
    $(function () {
        $('#select-car').selectize();
    });
    $(function () {
        $('#select-client').selectize();
    });
    $(function () {
        $('#select-time').selectize();
    });
</script>
<script src="/admin/vendors/moment/min/moment.min.js"></script>
<script src="/admin/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

</body>
</html>
