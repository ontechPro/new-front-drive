<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Drive-NSK</title>
    <?php include 'parts/styles.php'; ?>


</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php include 'parts/navBar.php'; ?>


                <div class="clearfix"></div>
                <br/>

                <!-- sidebar menu -->
                <?php include 'parts/sidebarMenu.php'; ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <?php include 'parts/footerButtons.php'; ?>

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php include 'parts/topBar.php'; ?>

        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Список заказов</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <a href="/admin/" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>
                                    Добавить</a>

                                <!-- start project list -->
                                <table class="table table-striped projects">
                                    <thead>
                                    <tr>
                                        <th class="w-3"># заказа</th>
                                        <th class="w-7">Машина</th>
                                        <th class="w-3">Клиент</th>
                                        <th>Начало</th>
                                        <th>Окончание</th>
                                        <th style="width: 7%">#Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $orders = ORM::forTable('orders')->order_by_desc('id')->findArray();
                                    foreach ($orders as $order) {
                                        $auto = ORM::forTable('auto')->where('znak', $order['znak'])->findOne();
                                        echo '
                                      <tr>
                                        <td class="w-3">' . $order['id'] . '</td>
                                        <td class="w-10">
                                            <a>' . $auto['model'] . ' \ ' . $order['znak'] . '</a>
                                                    </br>
                                        </td>     
                                           <td class="w-10">
                                            <a>' . $order['client'] . '</a>

                                        </td> 
                                         <td class="w-7">
                                            <a>' . $order['date_from'] . '</a>

                                        </td>                                          
                                        <td class="w-7">
                                            <a>' . $order['date_to'] . '</a>
                                        </td>';

                                        echo '
                                        <td class="w-5">
<!--                                            <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>-->
                                            <a href="addCar.php?id=' . $order['id'] . '" class="btn btn-default btn-xs w-100"><i class="fa fa-pencil"></i> Редактировать </a>
                                            <button type="button" class="btn btn-default btn-xs w-100" data-toggle="modal" data-target=".bs-example-modal-sm' . $order['id'] . '"><i class="fa fa-close"></i>Закрыть</button>

            <div class="modal fade bs-example-modal-sm' . $order['id'] . '" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h5 class="modal-title" id="myModalLabel2">Вы уверены, что хотите закрыть заказ номер ' . $order['id'] . '?</h5>
                  </div>
                  <div class="modal-body">
                  <form method="post" action="'.$_SERVER['PHP_SELF'].'">
                    <label for="coment' . $order['id'] . '" class="">Коменнтарий</label>
                    <input type="text" id="coment' . $order['id'] . '" class="form-control" placeholder="необязательное поле" name="comment">
                    
                    <label for="sum' . $order['id'] . '">Сумма заказа</label>
                    <input type="text" id="sum' . $order['id'] . '" class="form-control" required="" placeholder="' . rand(10, 60) * 100 . '" name="sum">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-danger" name="submit' . $order['id'] . '">Подтвердить</button>
                  </div>
                  </form>
                   
                </div>
              </div>
            </div>
            </td>
        </tr>';
                                        if (isset($_POST['submit' . $order['id']])) {
                                            $car = ORM::forTable('auto')->where('znak', $order['znak'])->findOne();
                                            $car->set('status', 0);
                                            $car->save();

                                            $closedOrder = ORM::for_table('closed')->create();
                                            $closedOrder->znak = $order['znak'];
                                            $closedOrder->comment = isset($_POST['comment']) ? $_POST['comment'] : '';
                                            $closedOrder->sum = $_POST['sum'];
                                            $closedOrder->client = $order['client'];
                                            $closedOrder->date_from = $order['date_from'];
                                            $closedOrder->date_to = $order['date_to'];
                                            $closedOrder->save();

                                            $client = ORM::forTable('orders')->where('znak', $order['znak'])->findOne();
                                            $client->delete();
                                            echo '<script> location.reload()</script>';
                                        }

                                    }

                                    ?>

                                    </tbody>
                                </table>
                                <!-- end project list -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- /footer content -->
    </div>
</div>
<?php include 'parts/scripts.php'; ?>


</body>
</html>
