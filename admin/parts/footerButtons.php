<?php
/**
 * Created by PhpStorm.
 * User: backend
 * Date: 15.12.16
 * Time: 14:21
 */
echo '<div class="sidebar-footer hidden-small">
              <a href="'.$_SERVER['PHP_SELF'].'" data-toggle="tooltip" data-placement="top" title="Обновить страницу">
                <i class="fa fa-refresh"></i>
              </a>
     
              <a href="mailto:backend@ontech.pro"    data-toggle="tooltip" data-placement="top" title="Сообщить об ошибке">
                 <i class="fa fa-bug"></i>
              </a>
              <a href="/" data-toggle="tooltip" data-placement="top" title="Выход">
                 <i class="fa fa-sign-out"></i>

              </a>
            </div>';