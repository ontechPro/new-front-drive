<?php
/**
 * Created by PhpStorm.
 * User: backend
 * Date: 15.12.16
 * Time: 14:17
 */
echo '<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Меню</h3>
                <ul class="nav side-menu">
                   <li><a><i class="fa fa-list-alt"></i> Заказы <span class="fa fa-chevron-down"></span></a>
                    <ul  class="nav child_menu">
                      <li><a href="/admin/orders.php">Список</a></li>
                      <li><a href="/admin/">Добавить заказ</a></li>
                      <li><a href="/admin/closed.php">Закрытые заказы</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-car"></i> Автомобили <span class="fa fa-chevron-down"></span></a>
                    <ul  class="nav child_menu">
                      <li><a href="/admin/cars.php">Список</a></li>
                      <li><a href="/admin/addCar.php">Добавить машину</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-group"></i> Клиенты <span class="fa fa-chevron-down"></span></a>
                    <ul  class="nav child_menu">
                      <li><a href="/admin/clients.php">Список</a></li>
                      <li><a href="/admin/addClient.php">Добавить клиента</a></li>
                    </ul>
                  </li>
                 <li><a><i class="fa fa-bar-chart"></i> Статистика <span class="fa fa-chevron-down"></span></a>
                    <ul  class="nav child_menu">
                      <li><a href="/admin/statistics.php">Статистика по автомобилям</a></li>
                    </ul>
                  </li>
                </ul>
              </div>';
//              <div class="menu_section">
//                <h3>Live On</h3>
//                <ul class="nav side-menu">
//                  <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
//                    <ul class="nav child_menu">
//                        <li><a href="#level1_1">Level One</a>
//                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
//                          <ul class="nav child_menu">
//                            <li class="sub_menu"><a href="level2.html">Level Two</a>
//                            </li>
//                            <li><a href="#level2_1">Level Two</a>
//                            </li>
//                            <li><a href="#level2_2">Level Two</a>
//                            </li>
//                          </ul>
//                        </li>
//                        <li><a href="#level1_2">Level One</a>
//                        </li>
//                    </ul>
//                  </li>
//                </ul>
//              </div>

        echo    '</div>';