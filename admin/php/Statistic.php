<?php

/**
 * Created by PhpStorm.
 * User: backend
 * Date: 29.12.16
 * Time: 14:37
 */
class Statistic
{
    private static $instance;
    private $monthIndex;
    private $year;
    private $auto;
    private $month = array();

    function __construct()
    {
        $this->month = array("1" => "Январь", "2" => "Февраль", "3" => "Март", "4" => "Апрель", "5" => "Май", "6" => "Июнь", "7" => "Июль", "8" => "Август", "9" => "Сентябрь", "10" => "Октябрь", "11" => "Ноябрь", "12" => "Декабрь");

        if (!isset($_SESSION['month'])) {
            $this->monTmp = date('m');
            if (substr($this->monTmp, 0, 1) == '0') {
                $this->monTmp = substr($this->monTmp, 1);
            }
            $_SESSION['month'] = $this->monTmp;
        }
        if (!isset($_SESSION['year'])) {
            $_SESSION['year'] = date('Y');
        }
        $this->monthIndex = $_SESSION['month'];
        $this->year = $_SESSION['year'];
        if (isset($_POST['+'])) {
            if ($this->monthIndex == 12) {
                $this->monthIndex = 1;
                $this->year++;
            } else {
                $this->monthIndex++;
            }
            header('Location: ' . $_SERVER['REQUEST_URI']);

        } elseif (isset($_POST['-'])) {
            if ($this->monthIndex == 1) {
                $this->monthIndex = 12;
                $this->year--;

            } else {
                $this->monthIndex--;
            }
            header('Location: ' . $_SERVER['REQUEST_URI']);
        }
        $_SESSION['month'] = $this->monthIndex;
        $_SESSION['year'] = $this->year;
        if (isset($_GET['auto'])) {
            $this->auto = $_GET['auto'];
        } else {
            $this->auto = 'Е632АС154';
        }
    }

    public static function getInstance()
    {
        return !(static::$instance instanceof self) ? static::$instance = new self() : static::$instance;
    }

    /**
     * @return array
     */
    public function getMonth($index)
    {
        return $this->month[$index];
    }

    public function printCarsButtons()
    {
        $result = '';
        $cars = ORM::for_table('auto')->findArray();// вытаскиваем все машины
        foreach ($cars as $car) { //бежим по ним и для каждой создаем форму со скрытым инпутом и по клику меняем адрес страницы под каждое авто
            $result .= '<form method="get" enctype="multipart/form-data" action="">
                            <input type="hidden" name="auto" value="' . $car['znak'] . '" >
                            <button id="' . $car['znak'] . '" class="w-100 btn" type="submit">' . $car['model'] . ' / ' . $car['color'] . ' / ' . $car['znak'] . '</button>
                        </form></br>';
        }
        echo $result;
    }

    public function printMonYearLabel()
    {
        echo $this->month[$_SESSION['month']] . '  ' . $_SESSION['year'];
    }

    public function getCalendarDate()
    {
        return $this->getYear() . '-' . $this->getMonthIndex() . '-' . date('d');
    }

    /**
     * @return false|string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @return int
     */
    public function getMonthIndex()
    {
        return $this->monthIndex;
    }

    /**
     * @param int $monthIndex
     */
    public function setMonthIndex($monthIndex)
    {
        $this->monthIndex = $monthIndex;
    }

    public function printCalendar()
    {
        $this->printWdayLabels();
        $result = '';
        $id = '';

        $number = cal_days_in_month(CAL_GREGORIAN, $_SESSION['month'], $_SESSION['year']);//получаем кол-во дей в текущеем месяце
        //     $number++;// не помню зачем увеличивал, без него работает лучше
        $d = 1; // в этой переменной лежит число, которое находится на кнопке (число дня в месяце)
        for ($i = 1; $i < 7; $i++) {// двойной цикл - печатаем календарик
            $result .= '<br>';// каждые 7 дней делаем перенос строки
            for ($j = 1; $j < 8; $j++) {
                if (strlen($d) < 2) { //к однозначным числам в начало добавляем нолик
                    $n = '0' . $d;
                } else {
                    $n = $d;
                }

                if (strlen($this->getMonthIndex()) < 2) {//если однозначное число, в начало добавляем 0
                    $this->setMonthIndex(0 . $this->getMonthIndex());
                }

                $str = $this->getYear() . '-' . $this->getMonthIndex() . '-' . $n;
                $today = date("wday", strtotime($str));
                $wday = substr($today, 0, 1);
                if ($wday > $j OR $d > $number) {// все пустые места до начала и после конца месяца затыкаем заглушками
                    $result .= '<button class="button1 info"  id="" style="opacity: 0; cursor: default; min-width: 50px; min-height: 50px" disabled> 00</button>';
                } else {//иначе печатаем кнопку календаря, подгружая при этом цвет кнопки из бд
                    if ($class = ORM::forTable('colors')->where('object_id', $str)->where('znak', $this->getAuto())->findOne()) {
                        $class = $class['class'];

                    } else {
                        $class = '';

                    }

                    $client = 'свободно';
                    $auto = $this->getAuto();
                    $dates = ORM::forTable('closed')->rawQuery("SELECT * FROM closed WHERE znak = '$auto'")->findMany();
                    foreach ($dates as $date) {
                        $isBetween = (strtotime($str) >= strtotime($date['date_from']) - 86400 && strtotime($str) <= strtotime($date['date_to']));
                        if ($isBetween) { //если машина была в сдаче, то красим кнопку зеленым
                            $class = 'btn-success';
                            $client = $date['client'];
                            $dataId = ORM::forTable('client')->where('fio',$client)->findOne();
                            $id = '(' . $dataId['id'] . ')';
                            //$id = '(' . $date['id'] . ')';

                            break;
                        } else {
                            $id = '';

                        }
                    }

                    $result .= '<button data-title="' . $client . ' ' . $id . '" style="min-width: 50px; min-height: 50px" class="button1 info btn ' . $class . '"  id="' . $str . '" > ' . $n . ' </button>';
                    $d++; //толькло в том случае, если мы напечатали кнопку, а не заглушку, тогда инкремируем
                }
            }
        }
        echo $result;
    }

    public function printWdayLabels()
    {
        $result = '';
        $days = [
            "1" => "пн",
            "2" => "вт",
            "3" => "ср",
            "4" => "чт",
            "5" => "пт",
            "6" => "сб",
            "7" => "вс",
        ];
        foreach ($days as $day) {//печатаем в календаре дни недели
            $result .= '<label class="btn font-size-20"   id="" style="min-width: 50px; min-height: 50px"> ' . $day . ' </label>';
        }
        echo $result;
    }

    /**
     * @return mixed
     */
    public function getAuto()
    {
        return $this->auto;
    }

    public function getTotalStat()
    {
        return ORM::forTable('closed')->sum('sum');
    }

    /**
     * @param string $carName
     * @return int
     */
    public function getCarStatTotal($carName)
    {
        if ($result = ORM::forTable('closed')->where('znak', $carName)->sum('sum')) {
            return $result;
        } else {
            return 0;
        }
    }

    /**
     * @param string $carName
     * @return int
     */
    public function getStatMon($date)
    {
        $dateTime = new DateTime($date);
        $month = $dateTime->format('m');
        $result = ORM::forTable('closed')->rawQuery("select SUM(sum) FROM closed WHERE MONTH(`date_to`) = '$month'")->findOne();
        if (is_string($result["SUM(sum)"])) {

            return $result["SUM(sum)"];
        } else {
            return 0;
        }
    }

    /**
     * @param string $carName
     * @param string $date
     * @return int
     */
    public function getCarStatMonth($carName, $date)
    {
        $dateTime = new DateTime($date);
        $month = $dateTime->format('m');
        $result = ORM::forTable('closed')->rawQuery("select SUM(sum) FROM closed WHERE MONTH(`date_to`) = '$month' AND znak='$carName'")->findOne();
        if (is_string($result["SUM(sum)"])) {

            return $result["SUM(sum)"];
        } else {
            return 0;
        }
    }

    public function getFullCarName($znak)
    {
        $car = ORM::for_table('auto')->where('znak', $znak)->findOne();// вытаскиваем все данные машины
        return $car['model'] . ' / ' . $car['color'] . ' / ' . $car['znak'];
    }


}

function getSumAllCarsForPeriod($dateStart, $dateFinish, $znak)
{

    $dateTimeStart = new DateTime($dateStart);
    $dateTimeFinish = new DateTime($dateFinish);
    //вытаскиваем все записи, которые хоть как-то цепляют заданный период для всех машин
    $carRow = ORM::forTable('closed')->rawQuery("SELECT * FROM `closed` WHERE (date_from BETWEEN '$dateStart' AND '$dateFinish' OR 
date_to BETWEEN '$dateStart' AND '$dateFinish' OR (date_from < '$dateStart' and date_to > '$dateFinish'))")->findMany();
    $result = 0;
    //проходим все результаты, и если запись входит в выбранный период только частично, то отсекам лишнее
    foreach ($carRow as $row) {
        $dateFrom = new DateTime(substr($row['date_from'], 0, 10));
        $dateTo = new DateTime(substr($row['date_to'], 0, 10));
        $interval = $dateFrom->diff($dateTo);
        if ($dateFrom <= $dateTimeStart) {
            $dateFrom = $dateTimeStart;
        }
        if ($dateTo >= $dateTimeFinish) {
            $dateTo = $dateTimeFinish;
        }
        $sumPerDay = $row['sum'] / $interval->format('%d'); //средняя прибыль в день
        $daysInPeriod = $dateFrom->diff($dateTo)->format('%d');//сколько дней входит в выбранный период

        $result += $daysInPeriod * $sumPerDay;
    }
    $res['forAll'] = $result;
    //всё то же самое для выбранной машины
    $carRow = ORM::forTable('closed')->rawQuery("SELECT * FROM `closed` WHERE znak = '$znak' AND (date_from BETWEEN '$dateStart' AND '$dateFinish' OR 
date_to BETWEEN '$dateStart' AND '$dateFinish' OR (date_from < '$dateStart' and date_to > '$dateFinish'))")->findMany();
    $result = 0;
    foreach ($carRow as $row) {
        $dateFrom = new DateTime(substr($row['date_from'], 0, 10));
        $dateTo = new DateTime(substr($row['date_to'], 0, 10));
        $interval = $dateFrom->diff($dateTo);
        if ($dateFrom <= $dateTimeStart) {
            $dateFrom = $dateTimeStart;
        }
        if ($dateTo >= $dateTimeFinish) {
            $dateTo = $dateTimeFinish;
        }
        $sumPerDay = $row['sum'] / $interval->format('%d');
        $daysInPeriod = $dateFrom->diff($dateTo)->format('%d');

        $result += $daysInPeriod * $sumPerDay;
    }
    $res["forCar"] = $result;

    echo json_encode($res);
}

//обработка аякс запроса
$input = file_get_contents('php://input');
$object = json_decode($input, TRUE);
if (isset($object["status"]) AND $object["status"] == 'getStats') {
    require_once '../database/idiorm.php';
    $date = explode(' - ', $object["period"]);

    $from = $date[0];
    $to = $date[1];
    getSumAllCarsForPeriod($from, $to, $object['znak']);
}