<?php
/**
 * Created by PhpStorm.
 * User: backend
 * Date: 16.12.16
 * Time: 14:11
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
if (isset($_GET['id'])){
    $id = $_GET['id'];
    $car = ORM::forTable('auto')->where('id', $id)->findOne();
    $model = $car['model'];
    $znak = $car['znak'];
    $vin = $car['vin'];
    $year = $car['year'];
    $color = $car['color'];
    $engine = $car['engine'];
    $power = $car['power'];
    $svid = $car['svid'];
    $status = $car['status'];

}else {
    $model = '';
    $znak = '';
    $vin = '';
    $year = '';
    $color = '';
    $engine = '';
    $power = '';
    $svid = '';
    $status = '';

}

if (isset($_POST['edit'])){

    $clientRow = ORM::for_table('auto')->find_one($_POST['id']);

    $clientRow->set('model', $_POST['model']);
    $clientRow->set('znak', $_POST['znak']);
    $clientRow->set('vin', $_POST['vin']);
    $clientRow->set('color', $_POST['color']);
    $clientRow->set('engine', $_POST['engine']);
    $clientRow->set('power', $_POST['power']);
    $clientRow->set('svid', $_POST['svid']);
    $clientRow->set('year', $_POST['year']);
    $clientRow->save();
    header('Location: /admin/cars.php');
}
if (isset($_POST['add'])){

    $clientRow = ORM::for_table('auto')->create();

    $clientRow->model = $_POST['model'];
    $clientRow->znak = $_POST['znak'];
    $clientRow->vin = $_POST['vin'];
    $clientRow->color = $_POST['color'];
    $clientRow->engine = $_POST['engine'];
    $clientRow->power = $_POST['power'];
    $clientRow->svid = $_POST['svid'];
    $clientRow->year = $_POST['year'];
    $clientRow->status = '0';
    $clientRow->save();
    header('Location: /admin/cars.php');
}