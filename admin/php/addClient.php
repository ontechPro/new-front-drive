<?php
/**
 * Created by PhpStorm.
 * User: backend
 * Date: 16.12.16
 * Time: 14:11
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
include '../database/idiorm.php';

if (isset($_POST['edit'])) {

    $clientRow = ORM::for_table('client')->find_one($_POST['id']);

    $clientRow->set('fio', $_POST['fio']);
    $clientRow->set('passport', $_POST['passport']);
    $clientRow->set('who', $_POST['who']);
    $clientRow->set('adres', $_POST['address']);
    $clientRow->set('prava', $_POST['prava']);
    $clientRow->set('phone', $_POST['phone']);
    $clientRow->set('id', $_POST['id']);
    $clientRow->set('birth', $_POST['birth']);
    $clientRow->save();

    if ($clientRow->save()) {
        header('Location: /admin/clients.php');
    } else {
        header('Location: www.ololo.com');

    }
} elseif (isset($_POST['add'])) {

    $clientRow = ORM::for_table('client')->create();

    $clientRow->fio = $_POST['fio'];
    $clientRow->passport = $_POST['passport'];
    $clientRow->who = $_POST['who'];
    $clientRow->adres = $_POST['address'];
    $clientRow->prava = $_POST['prava'];
    $clientRow->phone = $_POST['phone'];
    $clientRow->birth = $_POST['birth'];
    $clientRow->save();
    header('Location: /admin/clients.php');
}