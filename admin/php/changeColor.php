<?php
/**
 * Created by PhpStorm.
 * User: backend
 * Date: 26.12.16
 * Time: 12:45
 */
include_once '../database/idiorm.php';

$input = file_get_contents('php://input');
$object = json_decode($input, TRUE);

$object['class'] != null ?: $object['class'] = '';

//обработка аякса на изменения статуса кнопки в календаре
//если запись существует, мы её обновляем, иначе создаем новую
if (ORM::forTable('colors')->where(array('object_id' => $object['id'], 'znak' => $object['znak']))->findOne()) {
    $person = ORM::forTable('colors')->where(array('object_id' => $object['id'], 'znak' => $object['znak']))->findOne();
    $person->set('class', $object['class']);
    $person->set('znak', $object['znak']);

    $person->save();
} else {
    $person = ORM::forTable('colors')->create();
    $person->object_id = $object['id'];
    $person->class = $object['class'];
    $person->znak = $object['znak'];
    $person->save();
}
