<?php
/**
 * Created by PhpStorm.
 * User: backend
 * Date: 16.12.16
 * Time: 14:11
 */

require_once '../database/idiorm.php';
if ($_GET['id']) {
    $id = $_GET['id'];
    $order = ORM::forTable('auto')->findOne($id);
    $order->delete();
    header('Location: /admin/cars.php');
}