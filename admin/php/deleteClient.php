<?php
/**
 * Created by PhpStorm.
 * User: backend
 * Date: 16.12.16
 * Time: 14:11
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
include '../database/idiorm.php';
if ($_GET['id']) {
    $id = $_GET['id'];
    $client = ORM::forTable('client')->findOne($id);
    $client->delete();
    header('Location: /admin/clients.php');
}