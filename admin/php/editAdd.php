<?php
/**
 * Created by PhpStorm.
 * User: backend
 * Date: 16.12.16
 * Time: 14:11
 */
if (isset($_GET['id'])) {
    $title = 'Редактирование';
    $buttonText = 'Сохранить изменения';
    $id = $_GET['id'];
    $btnName = 'edit';
    $hidden = '';

} else {
    $title = 'Добавление';
    $buttonText = 'Сохранить';
    $id = '-1';
    $btnName = 'add';
    $hidden = 'hidden';
}
