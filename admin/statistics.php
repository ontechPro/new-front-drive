<?php
session_start();
include 'php/Statistic.php';
$stat = Statistic::getInstance();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Drive-NSK</title>

    <?php include 'parts/styles.php'; ?>
    <link href="/admin/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/admin/" class="site_title"><i class="fa fa-car"></i> <span>DRIVE-NSK</span></a>
                </div>

                <div class="clearfix"></div>
                <br/>

                <!-- sidebar menu -->
                <?php include 'parts/sidebarMenu.php'; ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <?php include 'parts/footerButtons.php'; ?>

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php include 'parts/topBar.php'; ?>

        <!-- /top navigation -->

        <!-- page content -->

        <div class="right_col" role="main" style="height: 1100px;">
            <!--            подключаем скрипты-->
            <?php include 'parts/scripts.php'; ?>

            <div class="col-md-4" style="min-width: 338px;">
                <?php $stat->printCarsButtons(); ?>
                <?php
                $value = date("Y-m-d") .'-'. (new DateTime(date("Y-m-d")))->add(new DateInterval("P3D"))->format('Y-m-d');
                $carFullName = $stat->getFullCarName($stat->getAuto());
                echo '
                <div class="col-md-12" style="padding:0; margin-bottom: 7px;"><h2>'.$carFullName.'</h2></div>
                <div class="col-md-12" style="padding:0; margin-bottom: 15px;">
                    <h2>Период:</h2>
                    <div class="control-group col-md-10" style="padding:0">
                        <div class="controls">
                            <div class="input-prepend input-group">
                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="text" name="reservation-time" id="reservation-time" class="form-control" value="'.$value.'" />
                            </div>
                        </div>
                    </div>
                    <button id="sendData" style="height: 34px; margin-left: 10px;">Ок!</button>
                </div>
                <br><br>';
                $carName = $stat->getAuto();
                echo '<h2 id="forCar">За выбранное время: ',$stat->getCarStatMonth($carName, $stat->getCalendarDate()), '</h2>';
                echo '<h2>За всё время: ',$stat->getCarStatTotal($carName), '</h2></br>';
                echo '<div class="col-md-12" style="padding:0; margin-bottom: 7px;"><h2>Все автомобили</h2></div>';
                echo "<h2 id='forAll'>Все машины за выбранное время: ", $stat->getStatMon($stat->getCalendarDate()), '</h2>';
                echo "<h2>Всего за всё время: ", $stat->getTotalStat(), '</h2>';
                ?>
            </div>
            <script>
                document.querySelector('#sendData').addEventListener('click', function () {
                    ajaxRequestGetMoney ();
                });

                function ajax (url, callback, obj) {
                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', url);
                    xhr.onreadystatechange = function(){
                        if (this.readyState == 4) {
                            if (this.status == 200)
                                callback(JSON.parse(this.responseText));
                            // иначе сетевая ошибка
                        }
                    };
                    xhr.send(JSON.stringify(obj));
                }

                function getAnswer(answer) {
                    console.log(answer);
                    document.querySelector('#forCar').innerHTML = 'За выбранное время:' + answer.forCar;
                    document.querySelector('#forAll').innerHTML = 'Все машины за выбранное время:' + answer.forAll;
                }

                function ajaxRequestGetMoney () {
                    var object = {};
                    object.period = document.getElementById('reservation-time').value;
                    object.status = 'getStats';
                    object.znak = '<?= $stat->getAuto() ?>';

                    ajax ('php/Statistic.php', getAnswer, object);

                }
            </script>

            <div class="col-md-5" style="width: 405px;">
                <div class="col-md-12 monSelector">

                    <form enctype="multipart/form-data" method="post">
                        <input type="hidden" value="-" name="-">
                        <button name="-" type="submit" class="btn btn-info"><i
                                class="fa fa-angle-double-left font-size-25" aria-hidden="true"></i></button>
                    </form>
                    <label for="" class="font-size-25"><?php $stat->printMonYearLabel() ?></label>
                    <form enctype="multipart/form-data" method="post">
                        <input type="hidden" value="+" name="+">
                        <button name="+" type="submit" class="btn btn-info"><i
                                class="fa fa-angle-double-right  font-size-25" aria-hidden="true"></i></button>
                    </form>

                </div>
                <?php $stat->printCalendar(); ?>
                <div class="col-md-6">


                    <button class="button2 btn btn-success" style="width: 364px;" id="">status - 1</button>
                    <button class="button2 btn btn-danger" style="width: 364px;" id="">status - 2</button>
                    <button class="button2 btn btn-primary" style="width: 364px;" id="">status - 3</button>
                </div>

            </div>

            <script> //этот скрипт меняем поочередно цвет кнопки и аяксом отправляем запрос на запись в бд
                function click(e) {
                    e.preventDefault();
                    var el = $(this);
                    if (el.hasClass('btn-primary')) {
                        el.removeClass("btn-primary");

                    } else if (el.hasClass("btn-danger")) {
                        //el.classList.remove("btn-warning");
                        el.addClass('btn-primary');

                        el.removeClass("btn-danger");
                    } else if (el.hasClass("btn-success")) {
                        el.addClass('btn-danger');
                        el.removeClass("btn-success");

                    } else {
                       // el.addClass('btn-success');
                        el.addClass('btn-danger');
                    }

                    myObj = {"id": this.id, "class": this.classList[3], "znak": "<?= $stat->getAuto() ?>"};
                    myJSON = JSON.stringify(myObj);

                    $.ajax({
                        type: 'POST',
                        url: 'php/changeColor.php',
                        data: myJSON,
                        success: function (data) {
                            $('.results').html(data);
                        }
                    });
                }

                $('.button1').on('click', click);
            </script>

        </div>
        <!-- /page content -->

        <!-- footer content -->

        <!-- /footer content -->
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#reservation').daterangepicker(null, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#reservation-time').daterangepicker({
            // timePicker: true,
            //timePickerIncrement: 30,

            locale: {
                format: 'YYYY-MM-DD',
                language: 'ru'
            }
        });
    });
</script>
<script src="/admin/vendors/moment/min/moment.min.js"></script>
<script src="/admin/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    $(document).ready(function () {
        $('#<?=$stat->getAuto()?>').addClass('btn-info');
    });

</script>

</body>
</html>
