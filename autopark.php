<!DOCTYPE html>
<html lang="ru">
<head>
    <?php
    include('block/head.php');
    ?>
    <link rel="stylesheet" href="/css/style-index.css">
    <link href="/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="/owl-carousel/owl.theme.css" rel="stylesheet">
</head>
<body>
<header>
    <?php
    include('block/header.php');
    ?>
</header>
<section class="autopark">
    <div>
        <div class="title-class">
            <div>Эконом класс</div>
        </div>
        <div class="autoslider">
            <div id="owl-demo1" class="owl-carousel owl-theme text-slider-news"></div>
        </div>
    </div>
    <div>
        <div class="title-class">
            <div>Средний класс</div>
        </div>
        <div class="autoslider">
            <div id="owl-demo2" class="owl-carousel owl-theme text-slider-news"></div>
        </div>
    </div>
    <div>
        <div class="title-class">
            <div>Бизнес класс</div>
        </div>
        <div class="autoslider">
            <div id="owl-demo3" class="owl-carousel owl-theme text-slider-news"></div>
        </div>
    </div>
</section>
<div class="hide itemcars">
    <?php foreach($cars as $car) : ?>
        <div data-car="<?= $car->folder ?>" class="item class<?= $car->class ?>">
            <a href="/car/<?= $car->folder ?>">
                <img src="/img/cars-mini/<?= $car->folder ?>.png">
            <div class="model"><span class="white-back"><?= $car->name ?></span></div>
            </a>
            <div class="hide">
                <span class="price"><?= $car->price5 ?></span>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php
include('block/popups.php');
?>
<script src="/owl-carousel/owl.carousel.js"></script>
<script>
    $( document ).ready(function () {
        var items1 = "";
        var items2 = "";
        var items3 = "";
        $('.itemcars .class1').each(function () {
            var datacar = $(this).attr('data-car');
            items1 = items1 + "<div data-car='"+datacar+"' class='item'>" + $(this).html() + "</div>";
        });
        $('.itemcars .class2').each(function () {
            var datacar = $(this).attr('data-car');
            items2 = items2 + "<div data-car='"+datacar+"' class='item'>" + $(this).html() + "</div>";
        });
        $('.itemcars .class3').each(function () {
            var datacar = $(this).attr('data-car');
            items3 = items3 + "<div data-car='"+datacar+"' class='item'>" + $(this).html() + "</div>";
        });
        $('#owl-demo2').html(items2);
        $('#owl-demo1').html(items1);
        $('#owl-demo3').html(items3);
        $("#owl-demo2").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 400,
            items: 3
        });
        $("#owl-demo1").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 400,
            items: 3
        });
        $("#owl-demo3").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 400,
            items: 3
        });
    });
</script>
</body>