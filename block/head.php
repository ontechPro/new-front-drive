<meta charset="UTF-8">
<?php
session_start();
$page = $_SERVER['REQUEST_URI'];
include('title.php');
?>
<title><?=$title ?></title>
<meta name="description" content="<?=$desc ?>">
<meta name="Keywords" content="<?=$keyw ?>">
<meta name="viewport" content="width=1280">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="/css/normalize.css">
<link rel="stylesheet" href="/css/fonts/stylesheet.css">
<link rel="stylesheet" href="/css/style.css">