<div class="logo">
	<a href="/">
		<img src="/img/logo.png">
	</a>
</div>
<div class="nav">
	<a <?php if($_SERVER['REQUEST_URI']=="/") echo "class=\"active\""; ?> href="/">Главная</a>
	<a <?php if($_SERVER['REQUEST_URI']=="/autopark") echo "class=\"active\""; ?>href="/autopark">Автопарк</a>
	<a <?php if($_SERVER['REQUEST_URI']=="/tariff") echo "class=\"active\""; ?>href="/tariff">Тарифы</a>
	<a <?php if($_SERVER['REQUEST_URI']=="/terms") echo "class=\"active\""; ?>href="/terms">Условия аренды</a>
	<a <?php if($_SERVER['REQUEST_URI']=="/services") echo "class=\"active\""; ?>href="/services">Услуги</a>
	<a <?php if($_SERVER['REQUEST_URI']=="/reviews") echo "class=\"active\""; ?>href="/reviews">Отзывы</a>
	<a <?php if($_SERVER['REQUEST_URI']=="/contacts") echo "class=\"active\""; ?>href="/contacts">Контакты</a>
</div>
<div class="phone-wrapper">
	<div class="phone">+7 983 510 78 21</div>
</div>
<?php if(!empty($_SESSION['message'])) {
	echo $_SESSION['message'];
}
require_once 'php/handlers/allcars.php';
?>