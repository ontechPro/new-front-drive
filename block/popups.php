<div class="popups">
    <input type="checkbox" name="book-popup" id="book-popup">
    <div class="book-popup">
        <div class="content">
            <div class="title">Для того, чтобы забронировать автомобиль,<br>
                оставьте Ваши контактые данные, и мы Вам перезвоним в ближайшее время.</div>
            <form method="post" action="/php/handlers/mail.php">
                <div class="form-group">
	                <input type="hidden" name="action" value="book" disabled required>
                    <select name="car" class="half-input car-input">
                        <?php foreach($cars as $car) : ?>
                            <option id="select<?= $car->folder ?>" <?php
                            if(!empty($oneCar) && $oneCar->folder==$car->folder) {
                                 echo "selected ";
                            }
                            ?>value="<?= $car->name ?>"><?= $car->name ?></option>
                        <?php endforeach; ?>
                    </select>
	                <div class="half-input calendar">Выберите даты аренды</div>
                    <div class="hide">
                        <div class="calendar">
                            <table id="calendar2" cellspacing="0" cellpadding="0">
                                <thead>
                                <tr><td>‹<td colspan="5"><td>›
                                <tr><td>Пн<td>Вт<td>Ср<td>Чт<td>Пт<td>Сб<td>Вс
                                <tbody>
                            </table>
                        </div>
                    </div>
                    <input type="hidden" name="date_from"><input type="hidden" name="date_to">
                </div>
                <div class="form-group">
                    <input type="text" name="name" placeholder="Ваше имя" required>
                </div>
                <div class="form-group">
                    <input type="number" name="phone" placeholder="Ваш телефон" required>
                </div>
                <div class="form-group">
                    <textarea name="comment" placeholder="Ваш комментарий" required></textarea>
                </div>
                <div class="send-form">
                    <button>Отправить</button>
                </div>
            </form>
        </div>
        <label for="book-popup" class="popup-shadow"></label>
    </div>
    <input type="checkbox" name="orientation" id="orientation-checkbox" <?php if(!empty($_SESSION['orientation'])){ echo "checked"; } ?>>
    <div class="cap">
        <div class="content">
            <div class="title">Рекомендуем повернуть ваше устройство горизонтально для более комфортного просмотра.</div>
        </div>
        <label for="orientation-checkbox" class="popup-shadow"></label>
    </div>
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter25387796 = new Ya.Metrika({
                    id:25387796,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/25387796" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script src="/js/jquery-3.1.1.min.js"></script>
<script src="/js/selectcar.js"></script>
<script src="/js/calendar.js"></script>