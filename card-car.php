<!DOCTYPE html>
<html lang="ru">
<head>
    <?php
    include ('block/head.php');
    ?>
</head>
<body>
<header>
    <?php
    include ('block/header.php');
    ?>
</header>
<div class="car-wrapper">
    <section class="card-car">
        <div>
            <div class="skewimg">
                <img src="/img/cars/<?=$oneCar->folder ?>/1.jpg">
            </div>
        </div>
        <div>
            <div class="info-car">
                <h1><?=$oneCar->name ?></h1>
                <div class="info">Данные автомобиля</div>
                <div class="block-info">
                    <div class="border">
                        <div><img src="/img/info-car1.png"></div>
                        <div><?=$oneCar->v ?> л.</div>
                    </div>
                    <div class="hide">Объём двигателя</div>
                </div>
                <div class="block-info">
                    <div class="border">
                        <div><img src="/img/info-car2.png"></div>
                        <div>Автомат</div>
                    </div>
                    <div class="hide">Коробка передач</div>
                </div>
                <div class="block-info">
                    <div class="border">
                        <div><img src="/img/info-car3.png"></div>
                        <div>Есть</div>
                    </div>
                    <div class="hide">Кондиционер</div>
                </div><br>
                <div class="block-info">
                    <div class="border">
                        <div><img src="/img/info-car4.png"></div>
                        <div>5</div>
                    </div>
                    <div class="hide">Количество мест</div>
                </div>
                <div class="block-info">
                    <div class="border">
                        <div><img src="/img/info-car5.png"></div>
                        <div>4</div>
                    </div>
                    <div class="hide">Количество дверей</div>
                </div>
                <div class="block-info">
                    <div class="border">
                        <div><img src="/img/info-car6.png"></div>
                        <div>Есть</div>
                    </div>
                    <div class="hide">Подогрев</div>
                </div>
                <div class="nav-tariff">
                    <input class="hide" type="radio" id="standard-tariff" name="type-tariff" checked>
                    <input class="hide" type="radio" id="standarptlus-tariff" name="type-tariff">
                    <input class="hide" type="radio" id="unlimited-tariff" name="type-tariff">
                    <label for="standard-tariff">
                        <div>Тариф “Стандарт”</div>
                        <div class="info">пробег <300 км в день
                            <div class="redline"></div>
                        </div>
                    </label>
                    <label for="standarptlus-tariff">
                        <div>Тариф “Стандарт +”</div>
                        <div class="info">пробег <500 км в день
                            <div class="redline"></div>
                        </div>
                    </label>
                    <label for="unlimited-tariff">
                        <div>Безлимитный тариф</div>
                        <div class="info">неограниченный пробег
                            <div class="redline"></div>
                        </div>
                    </label>
                </div>
                <div class="table">
                    <div class="head-table">
                        <div>1-3 сут.</div>
                        <div>4-7 сут.</div>
                        <div>8-15 сут.</div>
                        <div>16-30 сут.</div>
                        <div>> 31 сут.</div>
                    </div>
                    <div class="body-table">
                        <div class="standart-tarif"><?=$oneCar->price1 ?><div class="plus"></div><div class="ultimate"></div></div>
                        <div class="standart-tarif"><?=$oneCar->price2 ?><div class="plus"></div><div class="ultimate"></div></div>
                        <div class="standart-tarif"><?=$oneCar->price3 ?><div class="plus"></div><div class="ultimate"></div></div>
                        <div class="standart-tarif"><?=$oneCar->price4 ?><div class="plus"></div><div class="ultimate"></div></div>
                        <div class="standart-tarif"><?=$oneCar->price5 ?><div class="plus"></div><div class="ultimate"></div></div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div>
                <h2>Выберите даты аренды автомобиля</h2>
                <input type="hidden" name="date_from">
                <input type="hidden" name="date_to">
                <div class="calendar">
                    <table id="calendar2" cellspacing="0" cellpadding="0">
                        <thead>
                        <tr><td>‹<td colspan="5"><td>›
                        <tr><td>Пн<td>Вт<td>Ср<td>Чт<td>Пт<td>Сб<td>Вс
                        <tbody>
                    </table>
                </div>
            </div>
        </div>
        <div>
            <div class="skewimg">
                <img src="/img/cars/<?=$oneCar->folder ?>/2.jpg">
            </div>
        </div>
        <div>
            <div class="skewimg">
                <img src="/img/cars/<?=$oneCar->folder ?>/3.jpg">
            </div>
        </div>
        <div>
            <div class="skewimg">
                <img src="/img/cars/<?=$oneCar->folder ?>/4.jpg">
            </div>
        </div>
    </section>
</div>
<?php
include ('block/popups.php');
?>
<script src="/js/chengeprice.js"></script>
<script>
    $( document ).ready(function(){
        $('.form-group .hide .calendar').html('');
        $('.form-group>.calendar').attr('style','cursor:default');
    });
</script>
</body>