<!DOCTYPE html>
<html lang="ru">
<head>
    <?php
    include ('block/head.php');
    ?>
</head>
<body>
<header>
    <?php
    include ('block/header.php');
    ?>
</header>
<section class="contacts">
    <div id="map" class="map">
    </div>
    <div class="form-contacts">
        <div class="title">Наш офис располагается в Новосибирске по адресу ул. Фрунзе 86, офис 1101.<br>Однако, Вам даже не придется посещать его ведь заключение договора происходит в удобном для Вас месте в момент передачи автомобиля.
            <div class="center">Для связи с нами и аренды автомобиля можете связаться с нами одним из следующих способов:
            </div>
        </div>
        <div class="contacts-block-info">
            <div>
                <div>
                    <img src="img/phone.png">
                </div>
                <div>+7 (383) 510 78 21</div>
            </div>
        </div>
        <div class="contacts-block-info">
            <div>
                <div>
                    <img src="img/email.png">
                </div>
                <div>mail@drive-nsk.ru</div>
            </div>
        </div>
        <div class="title center">Либо заполнить форму обратной связи, и мы с Вами свяжемся<br> в ближайшее время</div>
        <form action="/php/handlers/mail.php" method="post">
            <input name="name" type="text" placeholder="Ваше имя" required>
            <input name="phone" type="number" placeholder="Номер телефона" required>
            <textarea name="text" placeholder="Ваш комментарий"></textarea>
            <button>Отправить<div></div></button>
        </form>
    </div>
</section>
<?php
include ('block/popups.php');
?>
<script src="http://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
<script type="text/javascript">
    DG.then(function() {
        var map;
        map = DG.map('map', {
            center: [55.03758, 82.933586],
            zoom: 17,
            scrollWheelZoom: false,
            fullscreenControl: false
        });
        DG.marker([55.03758, 82.9334]).addTo(map).bindPopup('ул. Фрунзе, 86, офис 1101. -  Drive');
    });
</script>
</body>