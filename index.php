<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
	include('block/head.php');
	?>
	<link rel="stylesheet" href="/css/style-index.css">
	<link href="/owl-carousel/owl.carousel.css" rel="stylesheet">
	<link href="/owl-carousel/owl.theme.css" rel="stylesheet">
</head>
<body>
<header>
	<?php
	include('block/header.php');
	?>
</header>
<section>
	<div class="content">
		<div class="class-avto">
			<span data-class="class1">Эконом<div></div></span>
			<span class="active" data-class="class2">Средний<div></div></span>
			<span data-class="class3">Бизнес<div></div></span>
		</div>
		<div class="price-model">
			<div class="model"></div>
			<div class="price"></div>
			<label for="book-popup" id="book" class="button">Забронировать<div></div></label>
			<a href="" id="more" class="button">Подробнее<div></div></a>
		</div>
		<div class="active-car">
			<img>
		</div>
	</div>
	<div class="slider">
		<div id="owl-demo2" class="owl-carousel owl-theme text-slider-news"></div>
		<div id="owl-demo1" class="owl-carousel owl-theme text-slider-news"></div>
		<div id="owl-demo3" class="owl-carousel owl-theme text-slider-news"></div>
	</div>
	<div class="hide itemcars">
		<?php foreach($cars as $car) : ?>
			<div data-car="<?= $car->folder ?>" class="item class<?= $car->class ?>">
				<img src="/img/cars-mini/<?= $car->folder ?>.png">
				<div class="model"><?= $car->name ?></div>
				<div class="hide">
					<span class="price"><?= $car->price5 ?></span>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>
<?php
include('block/popups.php');
?>
<script src="/owl-carousel/owl.carousel.js"></script>
<script src="/js/slide-car.js"></script>
</body>