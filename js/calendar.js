var dayfrom;
var dayto;
var firstDate = 0;
var funcACADactive = 0;
var today;
var selectDay;
var selectMonth;
var selectYear;
function addClassActiveDate(that){
    funcACADactive = 1;
    var daythat = $(that).html();
    today = $('.calendar .today').html();
    if(parseFloat(today) > parseFloat(daythat) && today != undefined){
        return false;
    }
    if(daythat == "&nbsp;" || daythat == ""){
        return false;
    }
    var selectedDate = $('.selected-date').html();
    var $set = $(that).parent().find('td');
    var n = $set.index(that);
    var activeClass = $(that).attr('class');
    if(activeClass == undefined){
        activeClass = "activ";
    }
    var tempday = dayfrom;
    if(selectedDate == undefined) {
        var blockMonthAndYear = $('#calendar2 thead td:nth-child(2)');
        var nowMonth = parseFloat(blockMonthAndYear.attr('data-month'))+1;
        var nowYear = parseFloat(blockMonthAndYear.attr('data-year'));
        tempday = 40;
        if(selectYear < nowYear) {
            tempday = 0;
        }else{
            if(selectMonth < nowMonth) {
                tempday = 0;
            }
        }
    }
    if (parseFloat(daythat) < parseFloat(tempday)) {
        var nextBlock = $(that);
        var i = n;
        while (activeClass.indexOf('selected-date') == "-1") {
            i++;
            nextBlock.addClass('active');
            if (i == 7) {
                i = 0;
                nextBlock = nextBlock.parent().next().find('td:first');
            } else {
                nextBlock = nextBlock.next();
            }
            activeClass = nextBlock.attr('class');
            if (activeClass == undefined) {
                activeClass = "activ";
            }
            innerPrevBlock = nextBlock.html();
            if(innerPrevBlock == "" || innerPrevBlock == "&nbsp;" || innerPrevBlock == undefined){
                activeClass = "selected-date";
            }
        }
        funcACADactive = 0;
        return false;
    }
    var prevBlock = $(that);
    var i = 7 - n;
    var innerPrevBlock;
    while(activeClass.indexOf('selected-date') == -1){
        prevBlock.addClass('active');
        if(i==7){
            i = 0;
            prevBlock = prevBlock.parent().prev().find('td:last');
        }else {
            prevBlock = prevBlock.prev();
        }
        activeClass = prevBlock.attr('class');
        if(activeClass == undefined){
            activeClass = "activ";
        }
        innerPrevBlock = prevBlock.html();
        if(innerPrevBlock == "" || innerPrevBlock == "&nbsp;" || innerPrevBlock == undefined){
            activeClass = "selected-date";
        }
        i++;
    }
    funcACADactive = 0;
}
function clearClassActiveDate(that) {
    $('.active').each(function () {
        $(this).removeClass('active');
    });
}
function getFirstDate(that){
    dayfrom = $(that).html();
    if(dayfrom == ""){
        return false;
    }
    today = $('.calendar .today').html();
    if(today == undefined) {
        var blockMonthAndYear = $('#calendar2 thead td:nth-child(2)');
        var nowMonth = parseFloat(blockMonthAndYear.attr('data-month'))+1;
        var nowYear = parseFloat(blockMonthAndYear.attr('data-year'));
        if(realYear > nowYear) {
            return false;
        }else{
            if(realMonth > nowMonth) {
                return false;
            }
        }
    }else{
        if(parseFloat(dayfrom) < parseFloat(today)) {
            return false;
        }
    }
    $(that).addClass('selected-date');
    var blockMonthAndYear = $('#calendar2 thead td:nth-child(2)');
    selectDay = parseFloat(dayfrom);
    selectMonth = parseFloat(blockMonthAndYear.attr('data-month'))+1;
    selectYear = parseFloat(blockMonthAndYear.attr('data-year'));
    firstDate = 1;
    var tempMonth = parseFloat(blockMonthAndYear.attr('data-month'))+1;
    if(tempMonth != 10 || tempMonth != 11 || tempMonth != 12){
        tempMonth = "0"+tempMonth;
    }
    var tempdayfrom = dayfrom;
    if(tempdayfrom.length == 1){
        tempdayfrom = "0"+tempdayfrom;
    }
    $('input[name=date_from]').val(blockMonthAndYear.attr('data-year')+"-"+tempMonth+"-"+tempdayfrom);
    $('.form-group>.calendar').html(tempdayfrom+"."+tempMonth+"."+blockMonthAndYear.attr('data-year'));
}
function getSecondDate(that){
    dayto = $(that).html();
    if(dayto == "&nbsp;" || dayto == ""){
        return false;
    }
    today = $('.calendar .today').html();
    if(today == undefined) {
        var blockMonthAndYear = $('#calendar2 thead td:nth-child(2)');
        var nowMonth = parseFloat(blockMonthAndYear.attr('data-month'))+1;
        var nowYear = parseFloat(blockMonthAndYear.attr('data-year'));
        if(realYear > nowYear) {
            return false;
        }else{
            if(realMonth > nowMonth) {
                return false;
            }
        }
    }else{
        if(parseFloat(dayto) < parseFloat(today)) {
            return false;
        }
    }
    $('.active').each(function(){
        $(this).removeClass('active');
    });
    $('.selected-date').removeClass('selected-date');
    var blockMonthAndYear = $('#calendar2 thead td:nth-child(2)');
    var tempMonth = parseFloat(blockMonthAndYear.attr('data-month'))+1;
    if(tempMonth != 10 || tempMonth != 11 || tempMonth != 12){
        tempMonth = "0"+tempMonth;
    }
    var tempdayto = dayto;
    if(tempdayto.length == 1){
        tempdayto = "0"+tempdayto;
    }
    $('input[name=date_to]').val(blockMonthAndYear.attr('data-year')+"-"+tempMonth+"-"+tempdayto);
    var tempBlockdate = $('.form-group>.calendar').html();
    $('.form-group>.calendar').html(tempBlockdate+" - "+tempdayto+"."+tempMonth+"."+blockMonthAndYear.attr('data-year'));
    firstDate = 0;
    selectDay = "";
    selectMonth = "";
    selectYear = "";
    $('#book-popup').prop('checked', true);
    $('.form-group>.calendar').next().attr('style','');
}
var realMonth;
var realYear;
function Calendar2(id, year, month) {
    var Dlast = new Date(year,month+1,0).getDate(),
        D = new Date(year,month,Dlast),
        DNlast = new Date(D.getFullYear(),D.getMonth(),Dlast).getDay(),
        DNfirst = new Date(D.getFullYear(),D.getMonth(),1).getDay(),
        calendar = '<tr>',
        month=["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
    if (DNfirst != 0) {
        for(var  i = 1; i < DNfirst; i++) calendar += '<td>';
    }else{
        for(var  i = 0; i < 6; i++) calendar += '<td>';
    }
    var classSelected;
    var havetoday = 0;
    for(var  i = 1; i <= Dlast; i++) {
        classSelected = "";
        if(selectYear == D.getFullYear() && selectMonth-1 == D.getMonth() && selectDay == i) {
            classSelected = "selected-date";
        }
        if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
            calendar += '<td class="today '+ classSelected + '">' + i;
            havetoday = 1;
        }else{
            calendar += '<td class="' + classSelected + '" >' + i;
        }
        if (new Date(D.getFullYear(),D.getMonth(),i).getDay() == 0) {
            calendar += '<tr>';
        }
    }
    for(var  i = DNlast; i < 7; i++) calendar += '<td>';
    document.querySelector('#'+id+' tbody').innerHTML = calendar;
    document.querySelector('#'+id+' thead td:nth-child(2)').innerHTML = month[D.getMonth()] +' '+ D.getFullYear();
    document.querySelector('#'+id+' thead td:nth-child(2)').dataset.month = D.getMonth();
    document.querySelector('#'+id+' thead td:nth-child(2)').dataset.year = D.getFullYear();
    if (document.querySelectorAll('#'+id+' tbody tr').length < 6) {  // чтобы при перелистывании месяцев не "подпрыгивала" вся страница, добавляется ряд пустых клеток. Итог: всегда 6 строк для цифр
        document.querySelector('#'+id+' tbody').innerHTML += '<tr><td><td><td><td><td><td><td>';
    }
    $('.calendar tbody td').click(function(){
        var that = this;
        if(firstDate==0){
            getFirstDate(that);
            return false;
        }
        getSecondDate(that);
    });
    $('.calendar tbody td').mouseenter(function(){
        if(firstDate==1){
            var that = this;
            addClassActiveDate(that);
        }
    });
    $('.calendar tbody td').mouseleave(function(){
        if(firstDate==1){
            var that = this;
            clearClassActiveDate(that);
        }
    });
    today = $('.calendar .today').html();
    if(today != undefined) {
        document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(1)').onclick = function () {
            return false;
        }
    }else{
        document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(1)').onclick = function () {
            Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month) - 1);
        }
    }
    if(havetoday==1){
        var blockMonthAndYear = $('#calendar2 thead td:nth-child(2)');
        realMonth = parseFloat(blockMonthAndYear.attr('data-month'))+1;
        realYear = parseFloat(blockMonthAndYear.attr('data-year'));
    }
}

$( document ).ready(function() {
    Calendar2("calendar2", new Date().getFullYear(), new Date().getMonth());
    // переключатель плюс месяц
    document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(3)').onclick = function () {
        Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month) + 1);
    };
    $('.form-group>.calendar').click(function () {
        $(this).next().fadeIn('slow');
    });
    $('label[for=orientation-checkbox]').click(function () {
        $.ajax({
            type: "POST",
            url: "/php/handlers/orientation"
        });
    });
});