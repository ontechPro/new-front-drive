/**
 * Created by Дмитрий on 13.01.2017.
 */
function chengeTarif(that) {
    var idthat = $(that).attr('id');
        switch(idthat) {
            case 'standard-tariff':
                $('.ultimate').each(function () {
                    $(this).attr('style','');
                });
                $('.plus').each(function () {
                    $(this).attr('style','');
                });
                break;
            case 'standarptlus-tariff':
                $('.ultimate').each(function () {
                    $(this).attr('style','');
                });
                $('.plus').each(function () {
                    $(this).attr('style','opacity:1');
                });
                break;
            case 'unlimited-tariff':
                $('.ultimate').each(function () {
                    $(this).attr('style','opacity:1');
                });
                $('.plus').each(function () {
                    $(this).attr('style','');
                });
                break;
        }
}
function calcTarif(){
    var price;
    var priceplus;
    $('.standart-tarif').each(function () {
        price = parseFloat($(this).html());
        priceplus = price*1.2/100;
        price = price*1.4/100;
        priceplus = Math.floor(priceplus)*100;
        price = Math.floor(price)*100;
        $(this).find('.ultimate').html(price);
        $(this).find('.plus').html(priceplus);
    });
}
$( document ).ready(function() {
    calcTarif();
    $('[name=type-tariff]').click(function () {
        var that = this;
        chengeTarif(that);
    });
});