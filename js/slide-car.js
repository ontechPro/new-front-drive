/**
 * Created by Дмитрий on 13.01.2017.
 */

function Showslider(classinner) {
    $('#owl-demo1').attr('style','opacity:0');
    $('#owl-demo2').attr('style','opacity:0');
    $('#owl-demo3').attr('style','opacity:0');
    classinner = classinner.slice(0, -11);
    switch(classinner) {
        case 'Эконом':
            $('#owl-demo1').attr('style','opacity:0; display:block;').attr('style','opacity:1; display:block;');
            var that = $('#owl-demo1 .item');
            goCar(that);
            chengeInfo(that);
            break;
        case 'Средний':
            $('#owl-demo2').attr('style','opacity:0; display:block;').attr('style','opacity:1; display:block;');
            var that = $('#owl-demo2 .item');
            goCar(that);
            chengeInfo(that);
            break;
        case 'Бизнес':
            $('#owl-demo3').attr('style','opacity:0; display:block;').attr('style','opacity:1; display:block;');
            var that = $('#owl-demo3 .item');
            goCar(that);
            chengeInfo(that);
            break;
    }
}
$( document ).ready(function () {
    $('.class-avto span').click(function () {
        $('.class-avto span').each(function(){
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        var stylesOwl1 = $('#owl-demo1').attr('style');
        var stylesOwl2 = $('#owl-demo2').attr('style');
        var stylesOwl3 = $('#owl-demo3').attr('style');
        if(stylesOwl1=="opacity:1; display:block;"){
            $('#owl-demo1').attr('style',"opacity:0; display:block;");
        }
        if(stylesOwl2=="opacity:1; display:block;"){
            $('#owl-demo2').attr('style',"opacity:0; display:block;");
        }
        if(stylesOwl3=="opacity:1; display:block;"){
            $('#owl-demo3').attr('style',"opacity:0; display:block;");
        }
        var classinner = $(this).html();
        setTimeout(Showslider, 200, classinner);

    });
    var items1 = "";
    var items2 = "";
    var items3 = "";
    $('.itemcars .class1').each(function () {
        var datacar = $(this).attr('data-car');
        items1 = items1 + "<div data-car='"+datacar+"' class='item'>" + $(this).html() + "</div>";
    });
    $('.itemcars .class2').each(function () {
        var datacar = $(this).attr('data-car');
        items2 = items2 + "<div data-car='"+datacar+"' class='item'>" + $(this).html() + "</div>";
    });
    $('.itemcars .class3').each(function () {
        var datacar = $(this).attr('data-car');
        items3 = items3 + "<div data-car='"+datacar+"' class='item'>" + $(this).html() + "</div>";
    });
    $('#owl-demo2').html(items2);
    $('#owl-demo1').html(items1);
    $('#owl-demo3').html(items3);
    $("#owl-demo2").owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        items: 3
    });
    $("#owl-demo2").queue(function() {
        adLisener();
        $(this).attr('style','opacity:1; display:block;');
    });
    $("#owl-demo1").owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        items: 3
    });
    $("#owl-demo1").queue(function() {
        adLisener();
        $(this).attr('style','');
    });
    $("#owl-demo3").owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        items: 3
    });
    $("#owl-demo3").queue(function() {
        adLisener();
        $(this).attr('style','');
    });
});
function goCar(that) {
    var img = $('.active-car img');
    img.attr('style','left:-100%');
    setTimeout(addNewCar, 500, that, img);
}
function addNewCar(that,img) {
    img.addClass('clear-transition');
    var srcimg = $(that).find('img').attr('src');
    var from = srcimg.indexOf('cars-mini/');
    var to = srcimg.length;
    srcimg = "img/cars-big/"+srcimg.substr(10+from, to);
    img.attr('style','left:100%');
    img.attr('src',srcimg);
    setTimeout(animateCar, 50, img);
}
function animateCar(img) {
    img.removeClass('clear-transition');
    $('.active-car img').attr('style','left:0');
    functime = 0;
}
function chengeInfo(that) {
    var infoBlock = $(that).parent();
    var model = infoBlock.find('.model').html();
    var price = infoBlock.find('.price').html();
    var maininfoblock = $('.price-model');
    maininfoblock.find('.model').html(model);
    maininfoblock.find('.price').html("от "+ price +"р./день");
    var folder = $(that).attr('data-car');
    $('#more').attr('href','/car/'+folder);
    chengeCar(folder);
}
var functime = 0;
function adLisener() {
    var that = $('.item');
    goCar(that);
    chengeInfo(that);
    $('.item').click(function () {
        if(functime==1){
            return false;
        }
        functime = 1;
        var that = this;
        goCar(that);
        chengeInfo(that);
    });
}