<?php
function superMail($from_name, $from_email, $to, $subject, $message = '', $filepath = '') {
	//Письмо с вложением состоит из нескольких частей, которые разделяются разделителем
	// генерируем разделитель
	$boundary = "--" . md5(uniqid(time()));

	// заголовки сообщения
	$headers = "MIME-Version: 1.0;\r\n";
	$headers .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n";// разделитель указывается в заголовке в параметре boundary
	$from = $from_name . ' <' . $from_email . '>';
	$headers .= "From: $from\r\n";
	$headers .= "Reply-To: $from\r\n";

	// первая часть - само сообщение
	$multipart = "--$boundary\r\n";
	$multipart .= "Content-Type: text/html; charset=windows-1251\r\n";
	$multipart .= "Content-Transfer-Encoding: base64\r\n";
	$multipart .= "\r\n";
	$multipart .= chunk_split(base64_encode(iconv("utf8", "windows-1251", $message)));

	// вторая часть - файл(ы) (если есть)
	if( ! empty($filepath)) {
		// Закачиваем файл(ы)
		if(is_array($filepath)) {
			foreach($filepath as $path) {
				$fp = fopen($path, "r");
				if( ! $fp) {
					print "Не удается открыть файл $path\n";
					exit();
				}
				$filename = pathinfo($path, PATHINFO_BASENAME);

				// чтение файла
				$file = fread($fp, filesize($path));
				fclose($fp);

				// второй частью прикрепляем файл, можно прикрепить два и более файла
				$message_part = "\r\n--$boundary\r\n";
				$message_part .= "Content-Type: application/octet-stream; name=\"$filename\"\r\n";
				$message_part .= "Content-Transfer-Encoding: base64\r\n";
				$message_part .= "Content-Disposition: attachment; filename=\"$filename\"\r\n";
				$message_part .= "\r\n";
				$message_part .= chunk_split(base64_encode($file));

				$multipart .= $message_part;
			}
		} else {
			$fp = fopen($filepath, "r");
			if( ! $fp) {
				print "Не удается открыть файл $filepath\n";
				exit();
			}
			$filename = pathinfo($filepath, PATHINFO_BASENAME);

			// чтение файла
			$file = fread($fp, filesize($filepath));
			fclose($fp);

			// второй частью прикрепляем файл, можно прикрепить два и более файла
			$message_part = "\r\n--$boundary\r\n";
			$message_part .= "Content-Type: application/octet-stream; name=\"$filename\"\r\n";
			$message_part .= "Content-Transfer-Encoding: base64\r\n";
			$message_part .= "Content-Disposition: attachment; filename=\"$filename\"\r\n";
			$message_part .= "\r\n";
			$message_part .= chunk_split(base64_encode($file));
			$message_part .= "\r\n--$boundary--\r\n";

			$multipart .= $message_part;
		}
	}

	return mail($to, $subject, $multipart, $headers);
}

/*$to       = user@email.com;
$from_name  = 'LikeHomes';
$from_email = 'info@likehomes.ru';
$subject    = "Заказ звонка";
if(superMail($from_name, $from_email, $to, $subject, $message)) {
	$_SESSION['message'] = '';
} else {
	$_SESSION['message'] = 'Ошибка при отправлении заявки!';
};*/