<?php
session_start();
if(!isset($_POST)) {
	exit('Вы не отправляете сообщение!');
}
if(empty($_POST['name']) || empty($_POST['phone']) || empty($_POST['comment'])) {
	$_SESSION['message'] = 'Заполните все поля формы!';
	exit;
}
$to         = 'mail@drive-nsk.ru';
$from_name  = $_POST['name'];
$from_email = 'no-reply@drive-nsk.ru';
$message    = 'Имя: ' . $_POST['name'] . "\r\n\r\n";
$message .= 'Телефон: ' . $_POST['phone'] . "\r\n\r\n";
$message .= 'Комментарий: ' . $_POST['comment'] . "\r\n\r\n";
if(isset($_POST['action']) && $_POST['action'] == 'book') {
	$subject = 'Заявка на аренду автомобиля';
	if(empty($_POST['car']) || empty($_POST['date_from']) || empty($_POST['date_to'])) {
		$_SESSION['message'] = 'Заполните все поля формы!';
		exit;
	}
	$message .= 'Арендуемый автомобиль: ' . $_POST['car'] . "\r\n\r\n";
	$date1 = $_POST['date_from'];
	$date2 = $_POST['date_to'];
	if($date1 > $date2) {
		$tmp   = $date2;
		$date2 = $date1;
		$date1 = $tmp;
	}
	$message .= 'Срок аренды: с ' . $date1 . ' по ' . $date2 . "\r\n\r\n";
} else {
	$subject = 'Связь с администрацией';
}
require '../functions/mail.php';
if(superMail($from_name, $from_email, $to, $subject, $message)) {
	$_SESSION['message'] = 'Ваша заявка принята! Ожидайте ответа от администрации!';
} else {
	$_SESSION['message'] = 'Ошибка при отправлении заявки!';
};
if(include '../functions/functions.php') {
	goBack();
} else {
	header("Location: /");
}