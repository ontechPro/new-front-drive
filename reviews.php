<!DOCTYPE html>
<html lang="ru">
<head>
    <?php
    include ('block/head.php');
    ?>
    <style>
        .contacts{
            background-image: url(../img/services-back.jpg);
            background-position: center 37%;
            background-repeat: no-repeat;
            background-size: 120%;
        }
        .contacts h1{
            margin-top: 43px;
            margin-left: auto;
            margin-right: auto;
            font-size: 18px;
            width: 1000px;
            height: 65px;
            font-family: 'proxima_nova_ltlight';
            text-align: left;
            font-weight: 100;
        }
        @media (max-width: 1800px) {
            .contacts h1 {
                margin-top: 27px;
            }
        }
    </style>
</head>
<body>
<header>
    <?php
    include ('block/header.php');
    ?>
</header>
<section class="contacts">
    <h1>Хотите высказаться о нашем сервисе? Предлагаем Вам сделать это незамедлительно. В настоящее время мы принимаем отзывы о нашей работе и пожелания любимых клиентов на Флампе. Будем рады получить любое мнение, а к критике мы относимся с пониманием и всегда готовы исправиться. Спасибо.</h1>
    <a href="http://novosibirsk.flamp.ru/firm/drive_kompaniya_po_prokatu_avtomobilejj-70000001006761474" class="button">Оставить отзыв</a>
    <div class="reviews">
        <a class="flamp-widget" href="http://novosibirsk.flamp.ru/firm/drive_kompaniya_po_prokatu_avtomobilejj-70000001006761474"  data-flamp-widget-type="responsive" data-flamp-widget-count="10" data-flamp-widget-id="70000001006761474" data-flamp-widget-width="100%">Отзывы о нас на Флампе</a><script>!function(d,s){var js,fjs=d.getElementsByTagName(s)[0];js=d.createElement(s);js.async=1;js.src="http://widget.flamp.ru/loader.js";fjs.parentNode.insertBefore(js,fjs);}(document,"script");</script>
    </div>
</section>
<?php
include ('block/popups.php');
?>
</body>