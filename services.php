<!DOCTYPE html>
<html lang="ru">
<head>
    <?php
    include ('block/head.php');
    ?>
</head>
<body>
<header>
    <?php
    include ('block/header.php');
    ?>
</header>
<section class="services">
    <h1>Услуги</h1>
    <div class="table">
        <div class="head-table">
            <div>Наименование услуги</div>
            <div>Цена</div>
        </div>
        <div class="body-table">
            <div>Доставка автомобиля в черте города:</div>
            <div></div>
        </div>
        <div class="second-level-body-table">
            <div>Центральный, Калининский, Заельцовский</div>
            <div>250 руб.</div>
        </div>
        <div class="second-level-body-table">
            <div>Октярьский р-ны, Ленинский, Кировский, Первомайский, Советский р-ны</div>
            <div>600 руб.</div>
        </div>
        <div class="second-level-body-table">
            <div>Аэропорт Толмачево</div>
            <div>600 руб.</div>
        </div>
        <div class="body-table">
            <div>Возврат вне офиса г.Новосибирска:</div>
            <div></div>
        </div>
        <div class="second-level-body-table">
            <div>Центральный, Калининский, Заельцовский</div>
            <div>250 руб.</div>
        </div>
        <div class="second-level-body-table">
            <div>Октярьский р-ны, Ленинский, Кировский, Первомайский, Советский р-ны</div>
            <div>600 руб.</div>
        </div>
        <div class="second-level-body-table">
            <div>Аэропорт Толмачево</div>
            <div>600 руб.</div>
        </div>
        <div class="body-table">
            <div>Задержка возврата автомобиля (час)</div>
            <div>300 руб.</div>
        </div>
        <div class="body-table">
            <div>Мойка автомобиля</div>
            <div>600 руб.</div>
        </div>
        <div class="body-table">
            <div>GPS-навигатор (в сутки)</div>
            <div>150 руб.</div>
        </div>
        <div class="body-table">
            <div>Перерасход бензина (за литр)</div>
            <div>40 руб.</div>
        </div>
        <div class="body-table">
            <div>Пролонгация договора вне офиса</div>
            <div>Бесплатно</div>
        </div>
        <div class="body-table">
            <div>Оформление дополнительного водителя</div>
            <div>Бесплатно</div>
        </div>
        <div class="body-table">
            <div>Бронирование автомобиля</div>
            <div>Бесплатно</div>
        </div>
        <div class="body-table">
            <div>Пролонгация договора аренды в офисе компании</div>
            <div>Бесплатно</div>
        </div>
    </div>
    <div class="info">*Стоимость доставки автомобиля в отдаленные районы г.Новосибирска уточняйте у оператора.</div>
    <div class="big-info">Так же, для Вашего удобства, Вы можете отправить копии паспорта и вод.удостоверения нам на почту и мы подготовим все необходимые документы заранее.</div>
</section>
<?php
include ('block/popups.php');
?>
</body>