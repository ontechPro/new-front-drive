<!DOCTYPE html>
<html lang="ru">
<head>
	<?php
	include('block/head.php');
	?>
</head>
<body>
<header>
	<?php
	include('block/header.php');
	include('php/handlers/allcars.php');
	?>
</header>
<section>
	<h1>Стоимость аренды</h1>
	<div class="nav-tariff">
		<input class="hide" type="radio" id="standard-tariff" name="type-tariff" checked>
		<input class="hide" type="radio" id="standarptlus-tariff" name="type-tariff">
		<input class="hide" type="radio" id="unlimited-tariff" name="type-tariff">
		<label for="standard-tariff">
			<div>Тариф “Стандарт”</div>
			<div class="info">пробег <300 км в день
				<div class="redline"></div>
			</div>
		</label>
		<label for="standarptlus-tariff">
			<div>Тариф “Стандарт +”</div>
			<div class="info">пробег <500 км в день
				<div class="redline"></div>
			</div>
		</label>
		<label for="unlimited-tariff">
			<div>Безлимитный тариф</div>
			<div class="info">неограниченный пробег
				<div class="redline"></div>
			</div>
		</label>
	</div>
	<div class="table">
		<div class="head-table">
			<div>Марка автомобиля</div>
			<div>1-3 сут.</div>
			<div>4-7 сут.</div>
			<div>8-15 сут.</div>
			<div>16-30 сут.</div>
			<div>> 31 сут.</div>
		</div>
		<?php foreach($cars as $car) : ?>
			<div class="body-table">
				<div data-car="<?= $car->folder ?>"><?= $car->name ?></div>
				<div class="standart-tarif"><?= $car->price1 ?><div class="plus"></div><div class="ultimate"></div></div>
				<div class="standart-tarif"><?= $car->price2 ?><div class="plus"></div><div class="ultimate"></div></div>
				<div class="standart-tarif"><?= $car->price3 ?><div class="plus"></div><div class="ultimate"></div></div>
				<div class="standart-tarif"><?= $car->price4 ?><div class="plus"></div><div class="ultimate"></div></div>
				<div class="standart-tarif"><?= $car->price5 ?><div class="plus"></div><div class="ultimate"></div></div>
				<div class="book"><label for="book-popup">Забронировать</label></div>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="info">*Все тарифы указаны в рублях. Тариф включает в себя: НДС, основные страховки. При открытии договора аренды с карты клиента удерживается сумма за аренду автомобиля и блокируется депозит.
	</div>
</section>
<?php
include('block/popups.php');
?>
<script src="js/chengeprice.js"></script>
<script>
	$(document).ready(function () {
		$('.book [for=book-popup]').click(function () {
			var folder = $(this).parent().parent().find('div:first').attr('data-car');
			chengeCar(folder);
		});
	})
</script>
</body>