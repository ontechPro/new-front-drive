<!DOCTYPE html>
<html lang="ru">
<head>
    <?php
    include ('block/head.php');
    ?>
</head>
<body>
<header>
    <?php
    include ('block/header.php');
    ?>
</header>
<section class="rent-terms">
        <h1>Условия аренды</h1>
    <div class="top-rent-terms">
        <div>Возраст – от 21 года
        </div>
        <div>Водительский стаж – от 3х лет
        </div>
        <div>Минимальный срок аренды - 2 суток
        </div>
    </div>
    <div class="big-info">В отдельных случаях, компания может предоставить
    автомобиль при меньшем возрасте или стаже вождения.</div>
    <div class="half-wrapper">
        <h2>Физическим лицам</h2>
        <h2>Корпоративным лицам</h2>
    </div>
    <div class="half-wrapper">
        <div class="info">
            <div class="first-title center">
                НЕОБХОДИМЫЕ ДОКУМЕНТЫ
            </div>
            <div class="half-wrapper">
                <div>
                    <div class="second-title">
                        Для граждан РФ:
                    </div>
                    <ul>
                        <li>Паспорт;</li>
                        <li>Водительское удостоверение;</li>
                        <li>Кредитная/дебетовая карта.</li>
                    </ul>
                </div>
                <div>
                    <div class="second-title">
                        Для иностранных граждан:
                    </div>
                    <ul>
                        <li>Паспорт иностранного гражданина с нотариально заверенным переводом;</li>
                        <li>Водительское удостоверение;</li>
                        <li>Кредитная/дебетовая карта;</li>
                        <li>Миграционная карта.</li>
                    </ul>
                </div>
            </div>
            <div class="first-title">
                ОПЛАТА:
            </div>
            <ul>
                <li>Банковский перевод, пластиковые карты.</li>
                <li>Оплата производится вперед за весь период проката автомобиля.</li>
                <li>При возврате раньше срока – производится перерасчет, исходя из фактического периода пользования автомобилем.</li>
                <li>В стоимость аренды автомобилей включены страховка по рискам ОСАГО и КАСКО, техническое обслуживание автомобиля.</li>
                <li>В стоимость аренды автомобиля не включены бензин, оплата стоянок и штрафов.</li>
            </ul>
            <div class="first-title">
                ДОПОЛНИТЕЛЬНО:
            </div>
            <ul>
                <li>Минимальный срок проката автомобиля: 2 суток (48 часов), с момента выдачи автомобиля.</li>
                <li>Регион эксплуатации: г. Новосибирск.</li>
                <li>Выезд за пределы г. Новосибирска, необходимо предварительно согласовать с представителем компании.</li>
                <li>В случае нарушения обозначенного в договоре маршрута, арендодатель имеет право обратиться в правоохранительные органы для начала процедуры розыска автомобиля.</li>
                <li>Арендодатель расценивает данную ситуацию как умышленное хищение автомобиля.</li>
                <li>Дополнительные водители оформляются бесплатно при наличии соответствующих документов.</li>
                <li>По окончании срока действия договора аренды, он может быть продлен по желанию Арендатора.</li>
                <li>При получении автомобиля в аренду вносится залог.</li>
                <li>Залог является гарантом выполнения Арендатором своих обязательств по соблюдению условий договора аренды автомобиля и возвращается после сдачи автомобиля.</li>
            </ul>
        </div>
        <div class="info">
            <div class="first-title">Компания «Drive» предлагает корпоративным клиентам прокат автомобиля без водителя с правом самостоятельного управления Арендатором на любой срок. В рамках предоставления данной услуги Компания обеспечивает:</div>
            <ul>
                <li>Предоставление определенного количества доверенностей на право управления автомобилем представителям Клиента;</li>
                <li>Страхование автомобилей КАСКО и ОСАГО;</li>
                <li>Специальные тарифы на услуги Компании в зависимости от количества автомобилей и срока оказания услуг;</li>
                <li>Тарифы без ограничения пробега и территориальной эксплуатации;</li>
                <li>Осуществление эвакуации и ремонта арендованного Клиентом автомобиля в случае поломки или аварии;</li>
                <li>Подменный автомобиль на время технического осмотра или ремонта предоставляемого автомобиля.</li>
            </ul>
            <div class="second-title">
                Для заключения договора с юридическим лицом необходимо предоставить следующие документы:
            </div>
            <ul>
                <li>Карточка предприятия с указанием фактического адреса.</li>
                <li>Копия свидетельства о регистрации.</li>
                <li>Копия свидетельства о постановке на налоговый учет.</li>
                <li>Копия решения участников/протокол об избрании (назначении) генерального директора.</li>
                <li>Копия приказа о вступлении в должность генерального директора.</li>
                <li>Доверенность от организации на право подписания договоров аренды и актов приема-передачи автомобиля, заверенная печатью предприятия и подписью генерального директора.</li>
            </ul>
        </div>
    </div>
    <a href="tariff.php" class="button">Арендовать</a>
</section>
<?php
include ('block/popups.php');
?>
</body>